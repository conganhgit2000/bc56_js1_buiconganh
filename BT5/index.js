/*
input:
    kySo
progress
    b1: khai báo biến chứa giá trị kySo
        
    b2: cho phép tính
        hangChuc = Math.floor(kySoEl % 10)
        hangDonVi = Math.floor(kySoEl / 10)

output:
    kết quả của Tổng ký số
*/



var kySo = 32; //ví dụ ta cho ký số là 32 thì tổng 2 ký số sẽ là 3 + 2

var hangChuc = Math.floor(kySo % 10);
var hangDonVi = Math.floor(kySo / 10);

//ta có công thức
var tongKySo = hangChuc + hangDonVi;

console.log("tongKySo:", tongKySo) // kết quả của phép tính là 5
