/*
input:
    chieuDai
    chieuRong

progress
    b1: khai báo biến chứa giá trị chieuDai và chieuRong
        
    b2: cho phép tính
        diện tích : dienTich = chieuDaiEl * chieuRongEl
        chu vi : chuVi = (chieuDaiEl + chieuRongEl) * 2

output:
    kết quả của diện tích và chu vi
*/

//ví dụ ta có chiều dài là 20 và chiều rộng 40
var chieuDai = 20;
var chieuRong = 40;

//ta có công thức tính chiều dài và chiều rộng
var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;


console.log("dienTich:", dienTich); // kết quả diện tích: 800
console.log("chuVi:", chuVi); // kết quả chu vi: 120

