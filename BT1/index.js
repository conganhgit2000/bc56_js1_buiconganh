/*
input:
    tienLuongMotNgay
    soNgayLamViec
progress
    b1: khai báo biến chứa giá trị tiền lương và số ngày làm việc
    b2: cho tongLuong = soNgayLamViec * tienLuong
output:
    tongLuong    
*/

var tienLuongMotNgay = 100; //tiền lương một ngày làm 100 ngàn đồng
var soNgayLamViec = 7; // tổng số ngày làm việc là 7 ngày

var tongLuong = tienLuongMotNgay * soNgayLamViec; 
console.log("tongLuong:", tongLuong); // tính ra tổng lương là 700 ngàn đồng
