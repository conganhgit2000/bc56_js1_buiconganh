/*
input:
    soThu1
    soThu2
    soThu3
    soThu4
    soThu5

progress
    b1: khai báo biến chứa giá trị
        soThu1
        soThu2
        soThu3
        soThu4
        soThu5
    b2: cho var ketQuaTB = (soThu1El + soThu2El + soThu3El + soThu4El + soThu5El) / 5

output:
    ketQuaTB   
*/


//gán giá trị số thực cho tất cả
var soThu1 = 10;
var soThu2 = 20;
var soThu3 = 30;
var soThu4 = 40;
var soThu5 = 50;

var ketQuaTB = (soThu1 + soThu2 + soThu3 + soThu4 + soThu5) / 5; //giá trị trung bình sẽ bằng tổng của 5 số chia cho 5

console.log("ketQuaTB:", ketQuaTB); //kết quả của phép tính là : 30


