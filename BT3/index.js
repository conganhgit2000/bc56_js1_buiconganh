/*
input:
    giaTienUSD
    soTienUSD
progress
    b1: khai báo biến chứa giá trị giá tiền USD khi quy đổi ra tiền VND và số tiền USD cần đổi
    b2: cho tongTienVND = giaTienUSD * soTienUSD
output:
    tongTienUSD   
*/

var giaTienUSD = 23.5; //ví dụ giá tiền USD hiện nay là 23.500 VND
var soTienUSD = 2; // bạn muốn đổi 2 USD sang VND
var tongTienVND = giaTienUSD * soTienUSD; //Ta có phép tính

console.log("tongTienVND:", tongTienVND); //kết quả của phép tính trên là 47 ngàn đồng 

